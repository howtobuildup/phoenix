"use client";

/* eslint-disable no-param-reassign */
/* eslint-disable react/no-array-index-key */

import {
	TextInput,
	Button,
	Table,
	Tooltip,
	ActionIcon,
	Space,
	Divider,
} from "@mantine/core";
import {
	IconTrash,
	IconPlus,
	IconCheck,
	IconInfoCircle,
	IconChevronDown,
	IconChevronUp,
} from "@tabler/icons";
import { useState, useEffect, ChangeEvent, useCallback } from "react";
import { useParams, useRouter } from "next/navigation";
import { useTranslate } from "@refinedev/core";
import { classifierService } from "src/services";
import { showNotification } from "@mantine/notifications";
import ClassifierViewBreadcrumb from "@components/breadcrumbs/classifierView";
import { normaliseErrorMessage } from "src/utils";

// Define types for class and keyword group structures
interface ClassData {
	id?: number;
	tempId?: number;
	name: string;
	description: string;
}

interface KeywordGroup {
	id?: number; // Will exist for existing configurations, undefined for new ones
	tempId?: number;
	class_id: number;
	musts: string;
	nots?: string;
	class_name?: string;
}

const EditKeywordClassifier: React.FC = () => {
	const router = useRouter();
	const translate = useTranslate();
	const { projectid, id } = useParams();
	// State to manage classes and keyword groups
	const [classifierName, setClassifierName] = useState("");
	const [classifierDescription, setClassifierDescription] = useState("");
	const [classifier, setClassifier] = useState<any>();
	const [classes, setClasses] = useState<ClassData[]>([]);
	const [keywordGroups, setKeywordGroups] = useState<KeywordGroup[]>([]);
	const [isModified, setIsModified] = useState<boolean>(false);
	const [isBasicModified, setIsBasicModified] = useState<boolean>(false);
	const [refetch, setRefetch] = useState<boolean>(true);
	const [openRows, setOpenRows] = useState<{ [key: number]: boolean }>({});

	// Debounce utility
	const debounce = <T extends (...args: any[]) => void>(
		func: T,
		delay: number
	): ((...args: Parameters<T>) => void) => {
		let timeout: ReturnType<typeof setTimeout>;
		return (...args: Parameters<T>) => {
			clearTimeout(timeout);
			timeout = setTimeout(() => func(...args), delay);
		};
	};

	const showRow = (index: number) => {
		setOpenRows((prev) => ({ ...prev, [index]: true }));
	};

	const toggleRow = (index: number) => {
		setOpenRows((prev) => ({ ...prev, [index]: !prev[index] }));
	};

	const navigateWithConfirmation = (url: string) => {
		if (isModified || isBasicModified) {
			const confirmed = window.confirm(
				translate("notifications.unsaved_changes")
			);
			if (confirmed) {
				router.push(url); // Proceed with the navigation if confirmed
			}
		} else {
			router.push(url); // Directly navigate if no unsaved changes
		}
	};

	// Fetch initial data on mount
	const fetchData = useCallback(async () => {
		try {
			const { data } = await classifierService.getClassifierData({
				project_id: projectid as string,
				classifier_id: id as string,
			});
			setClassifier(data);
			setClassifierName(data?.name);
			setClassifierDescription(data?.description);
			// Set classes and keyword groups from API response
			setClasses(data.intermediatory_classes);
			setKeywordGroups(data.intermediatory_class_to_keyword_configs);
			setRefetch(false);
		} catch (error) {
			console.error("Error fetching classifier data", error);
		}
	}, [id, projectid]);

	useEffect(() => {
		if (id && projectid && refetch) {
			fetchData();
		}

		// Warn user on exit without saving
		window.onbeforeunload = isModified || isBasicModified ? () => true : null;

		return () => {
			window.onbeforeunload = null;
		};
	}, [isModified, isBasicModified, id, projectid, fetchData, refetch]);

	// Input change handlers
	const handleClassChange = (
		index: number,
		field: "name" | "description",
		value: string
	): void => {
		const updatedClasses = classes.map((cls, i) =>
			i === index ? { ...cls, [field]: value } : cls
		);
		setClasses(updatedClasses);
		setIsModified(true);
	};

	// Auto-save class data
	const autoSaveClass = debounce(async (index: number) => {
		const updatedClass = classes[index];
		try {
			if (updatedClass.id && updatedClass.name) {
				await classifierService.updateClassifierClassData(
					{
						project_id: projectid,
						classifier_id: id,
						class_id: updatedClass.id,
					},
					{
						name: updatedClass.name,
						description: updatedClass.description,
					}
				);
			} else if (updatedClass.name) {
				await classifierService.createClassifierClassData(
					{ project_id: projectid, classifier_id: id },
					{
						name: updatedClass.name,
						description: updatedClass.description,
					}
				);
			} else {
				return;
			}
			showNotification({
				title: translate("status.success"),
				message: translate("classifiers.success.success"),
			});
		} catch (error) {
			console.error("Error saving class data", error);
			showNotification({
				title: "Error",
				color: "red",
				message: normaliseErrorMessage(error, translate),
			});
		} finally {
			setRefetch(true);
			setIsModified(false); // Reset the modified flag after saving
		}
	}, 500); // Auto-save delay of 500ms after the user stops typing

	const handleKeywordChange = (groupId: number, value: string): void => {
		const updatedKeywordGroups = keywordGroups.map((group) =>
			group.id === groupId || group.tempId === groupId
				? {
						...group,
						musts: value,
					}
				: group
		);
		setKeywordGroups(updatedKeywordGroups);
		setIsModified(true);
	};

	// Auto-save for keyword group changes
	const autoSaveKeywordGroup = debounce(async (groupId: number) => {
		const updatedGroup = keywordGroups.find(
			(group) => group.id === groupId || group.tempId === groupId
		);
		if (!updatedGroup) return;

		try {
			if (updatedGroup?.id && updatedGroup?.musts) {
				await classifierService.updateKeywordClassifierConfig(
					{
						project_id: projectid,
						classifier_id: id,
						config_id: updatedGroup.id,
					},
					{
						class_id: updatedGroup.class_id,
						musts: updatedGroup.musts,
						nots: updatedGroup.nots || "",
					}
				);
			} else if (updatedGroup?.id && updatedGroup?.musts === "") {
				// This means the user has made a input empty that they created earlier
				// we delete this as we presume this is what the user wanted
				await classifierService.removeKeywordClassifierConfig({
					project_id: projectid,
					classifier_id: id,
					config_id: updatedGroup.id,
				});
			} else if (updatedGroup?.musts) {
				// This means the user has created a new keyword group
				// Cannot be created if the musts is empty
				await classifierService.createKeywordClassifierConfig(
					{
						project_id: projectid,
						classifier_id: id,
						config_id: updatedGroup.id,
					},
					{
						class_id: updatedGroup.class_id,
						musts: updatedGroup.musts,
						nots: updatedGroup.nots || "",
					}
				);
			}
		} catch (error: any) {
			showNotification({
				title: "Error",
				color: "red",
				message: normaliseErrorMessage(error, translate),
			});
			console.error("Error submitting class", error);
		} finally {
			setRefetch(true);
			setIsModified(false); // Reset the modified flag after saving
		}
	}, 500); // Auto-save delay of 500ms after the user stops typing

	const handleUpdateBasicInfo = async () => {
		try {
			await classifierService.updateClassifierBasicData(
				{
					project_id: projectid,
					classifier_id: id,
				},
				{
					name: classifierName,
					description: classifierDescription,
				}
			);
			setIsBasicModified(false);
			showNotification({
				title: translate("status.success"),
				message: translate("classifiers.success.success"),
			});
		} catch (error) {
			console.error("Error applying classifier", error);
		}
	};

	const handleAddKeywordGroup = (class_id: number): void => {
		setKeywordGroups((prev) => [
			...prev,
			{ tempId: Math.random(), class_id, musts: "" },
		]);
		setIsModified(true);
	};

	const handleRemoveKeywordGroup = async (groupId: number): Promise<void> => {
		try {
			const groupToRemove = keywordGroups.find(
				(group) => group.id === groupId || group.tempId === groupId
			);
			if (groupToRemove?.id) {
				await classifierService.removeKeywordClassifierConfig({
					project_id: projectid,
					classifier_id: id,
					config_id: groupToRemove.id,
				});
				setRefetch(true);
			} else {
				setKeywordGroups((prev) => prev.filter((g) => g.tempId !== groupId));
			}
		} catch (error) {
			console.error("Error removing keyword group", error);
		}
	};

	const handleAddClass = (): void => {
		setClasses((prev) => [
			...prev,
			{ tempId: Math.random(), name: "", description: "" },
		]);
		setIsModified(true);
	};

	const handleRemoveClass = async (index: number): Promise<void> => {
		try {
			const classToRemove = classes[index];
			if (classToRemove?.id) {
				await classifierService.removeClassifierClassData({
					project_id: projectid,
					classifier_id: id,
					class_id: classToRemove.id,
				});
				setRefetch(true);
			} else {
				setClasses((prev) => prev.filter((_, i) => i !== index));
				setKeywordGroups((prev) =>
					prev.filter((group) => group.class_id !== classToRemove.tempId)
				);
			}
		} catch (error) {
			showNotification({
				title: translate("status.error"),
				color: "red",
				message: normaliseErrorMessage(error, translate),
			});
			console.error("Error removing class:", error);
		}
	};

	return (
		<div className="p-8 bg-white min-h-screen">
			<ClassifierViewBreadcrumb
				record={classifier}
				projectid={projectid as string}
			/>
			<h1 className="text-2xl font-semibold">
				{translate("classifiers.types.keyword_match.edit")}
			</h1>
			<p className="mb-4">
				{translate("classifiers.types.keyword_match.edit_description")}
			</p>

			<Space h="md" />
			<div>
				<Divider
					my="sm"
					label={translate(
						"classifiers.types.keyword_match.view.accordion.basic_setup"
					)}
				/>
				<TextInput
					label="Name"
					placeholder={translate("classifiers.fields.name_placeholder")}
					value={classifierName}
					onChange={(e) => {
						setIsBasicModified(true);
						setClassifierName(e.target.value);
					}}
					required
				/>
				<Space h="sm" />
				<TextInput
					label="Description"
					placeholder={translate("classifiers.fields.description_placeholder")}
					value={classifierDescription}
					onChange={(e) => {
						setIsBasicModified(true);
						setClassifierDescription(e.target.value);
					}}
					required
				/>
				<Space h="sm" />
				<div className="flex justify-end">
					<Button
						leftIcon={<IconCheck size={16} />}
						mt="sm"
						onClick={handleUpdateBasicInfo}
					>
						{translate("buttons.update_basic_info")}
					</Button>
				</div>
			</div>
			<Space h="lg" />

			{/* Classes Section */}
			<Divider
				my="sm"
				label={translate(
					"classifiers.types.keyword_match.view.accordion.configuration"
				)}
			/>
			<Table highlightOnHover withBorder>
				<thead>
					<tr>
						<th>{translate("classifiers.fields.class_name")}</th>
						<th>{translate("projects.fields.description")}</th>
						<th className="flex items-center justify-center">
							{translate("classifiers.fields.keywords")}
							<Tooltip
								width={350}
								multiline
								label={translate("classifiers.info.create_keywords")}
							>
								<span className="flex">
									<IconInfoCircle size={12} />
								</span>
							</Tooltip>
						</th>
						<th>{translate("table.actions")}</th>
					</tr>
				</thead>
				<tbody>
					{classes?.map((classItem, classIndex) => (
						<tr key={classIndex}>
							<td className="align-baseline">
								<TextInput
									placeholder={translate(
										"classifiers.fields.class_name_placeholder"
									)}
									value={classItem.name}
									onChange={(event: ChangeEvent<HTMLInputElement>) => {
										handleClassChange(classIndex, "name", event.target.value);
										setIsModified(true);
									}}
									onBlur={() => {
										autoSaveClass(classIndex);
									}}
								/>
							</td>
							<td className="align-baseline">
								<TextInput
									placeholder={translate(
										"classifiers.fields.class_description_placeholder"
									)}
									value={classItem.description}
									onChange={(event: ChangeEvent<HTMLInputElement>) => {
										handleClassChange(
											classIndex,
											"description",
											event.target.value
										);
										setIsModified(true);
									}}
									onBlur={() => {
										autoSaveClass(classIndex);
									}}
								/>
							</td>
							<td className="!pt-0">
								<Table>
									<tbody className="flex flex-col items-center">
										{openRows[classIndex]
											? keywordGroups
													?.filter(
														(group) =>
															group.class_id === classItem?.id ||
															group.class_id === classItem?.tempId
													)
													.map((keywordGroup, keywordIndex) => (
														<tr key={keywordIndex}>
															<td>
																<TextInput
																	placeholder={translate(
																		"classifiers.fields.keywords_placeholder"
																	)}
																	value={keywordGroup.musts}
																	onChange={(
																		event: ChangeEvent<HTMLInputElement>
																	) => {
																		handleKeywordChange(
																			(keywordGroup.id ?? keywordGroup.tempId)!,
																			event.target.value
																		);
																		setIsModified(true);
																	}}
																	onBlur={() => {
																		autoSaveKeywordGroup(
																			(keywordGroup.id ?? keywordGroup.tempId)!
																		);
																	}}
																/>
															</td>
															<td>
																<Tooltip
																	label={translate(
																		"classifiers.actions.tooltips.delete_keyword"
																	)}
																>
																	<ActionIcon
																		color="red"
																		variant="light"
																		onClick={() =>
																			handleRemoveKeywordGroup(
																				(keywordGroup.id ??
																					keywordGroup.tempId)!
																			)
																		}
																	>
																		<IconTrash size={16} />
																	</ActionIcon>
																</Tooltip>
															</td>
														</tr>
													))
											: `${
													keywordGroups?.filter(
														(group) =>
															group.class_id === classItem?.id ||
															group.class_id === classItem?.tempId
													).length
												} ${translate("classifiers.keyword_configurations")}`}
										<div className="flex gap-4">
											<Tooltip
												label={translate(
													"classifiers.actions.tooltips.add_keyword"
												)}
											>
												<ActionIcon
													color="blue"
													variant="light"
													onClick={() => {
														showRow(classIndex);
														handleAddKeywordGroup(
															(classItem.tempId ?? classItem.id)!
														);
													}}
												>
													<IconPlus size={16} />
												</ActionIcon>
											</Tooltip>
											{keywordGroups.filter(
												(group) =>
													group.class_id === classItem?.id ||
													group.class_id === classItem?.tempId
											).length > 0 && (
												<ActionIcon
													color="dark"
													variant="light"
													onClick={() => toggleRow(classIndex)}
												>
													{openRows[classIndex] ? (
														<IconChevronUp size={16} />
													) : (
														<IconChevronDown size={16} />
													)}
												</ActionIcon>
											)}
										</div>
									</tbody>
								</Table>
							</td>
							<td className="align-baseline">
								<div className="w-full h-full flex gap-1 items-center justify-center">
									<Tooltip
										label={translate(
											"classifiers.actions.tooltips.delete_class"
										)}
									>
										<ActionIcon
											color="red"
											variant="light"
											onClick={() => handleRemoveClass(classIndex)}
										>
											<IconTrash size={16} />
										</ActionIcon>
									</Tooltip>
								</div>
							</td>
						</tr>
					))}
					<div className="flex gap-2 w-full m-2">
						<Button
							leftIcon={<IconPlus size={16} />}
							variant="subtle"
							onClick={handleAddClass}
						>
							{translate("buttons.add_class")}
						</Button>
					</div>
				</tbody>
			</Table>

			<Space h="lg" />
			<div className="flex justify-end gap-2 w-full">
				<Button
					mt="sm"
					disabled={!classifierName}
					onClick={() => {
						if (classifier)
							navigateWithConfirmation(
								`/projects/${projectid}/classifiers/${classifier.type}/${classifier.id}`
							);
					}}
				>
					{translate("classifiers.buttons.finish")}
				</Button>
			</div>
		</div>
	);
};

export default EditKeywordClassifier;
