"use client";

import React, { useState } from "react";
import { useShow, useTranslate } from "@refinedev/core";
import { Show } from "@refinedev/mantine";
import {
	Accordion,
	ActionIcon,
	Container,
	Table,
	TextInput,
	Title,
} from "@mantine/core";
import { useParams } from "next/navigation";
import { IconChevronDown, IconChevronUp } from "@tabler/icons";
import ClassifierViewBreadcrumb from "@components/breadcrumbs/classifierView";
import ClassifierViewStatus from "@components/classifier/view-status";
import ClassifierViewGeneral from "@components/classifier/view-general";
import ClassifierRunModal from "@components/modals/classifier-run";
import ClassifierViewHeader from "@components/classifier/view-header";

export default function KeywordClassifierShow(): JSX.Element {
	const { projectid, id } = useParams();
	const [opened, setOpened] = useState(false);
	const translate = useTranslate();
	const [openRows, setOpenRows] = useState<{ [key: number]: boolean }>({});
	const { queryResult } = useShow({
		resource: `projects/${projectid}/classifiers`,
		id: id as string,
	});

	const { data, isLoading, refetch } = queryResult;

	const record = data?.data;

	const toggleRow = (index: number) => {
		setOpenRows((prev) => ({ ...prev, [index]: !prev[index] }));
	};

	return (
		<Show
			title={<Title order={3}>{record?.name}</Title>}
			breadcrumb={
				<ClassifierViewBreadcrumb
					record={record}
					projectid={projectid as string}
				/>
			}
			isLoading={isLoading}
			headerButtons={() => null}
		>
			<ClassifierViewHeader
				id={id as string}
				record={record}
				setOpened={setOpened}
			/>
			<div className="w-full">
				<Accordion
					styles={{
						control: {
							paddingLeft: 0,
						},
						item: {
							"&[data-active]": {
								backgroundColor: "none",
							},
						},
					}}
					multiple
					defaultValue={["status", "general", "configuration"]}
				>
					<Accordion.Item value="status" className="mb-4">
						<Accordion.Control>
							<Title order={5}>
								{translate(
									"classifiers.types.keyword_match.view.accordion.status"
								)}
							</Title>
						</Accordion.Control>
						<Accordion.Panel>
							<ClassifierViewStatus record={record} />
						</Accordion.Panel>
					</Accordion.Item>
					<Accordion.Item value="general" mb="md">
						<Accordion.Control>
							<Title order={5}>
								{translate(
									"classifiers.types.keyword_match.view.accordion.general"
								)}
							</Title>
						</Accordion.Control>
						<Accordion.Panel>
							<ClassifierViewGeneral record={record} />
						</Accordion.Panel>
					</Accordion.Item>
					<Accordion.Item value="configuration" mb="md">
						<Accordion.Control>
							<Title order={5}>
								{translate(
									`classifiers.types.keyword_match.view.accordion.configuration`
								)}
							</Title>
						</Accordion.Control>
						<Accordion.Panel>
							<Container className="mx-0 flex flex-col my-4">
								<Table highlightOnHover withBorder>
									<thead>
										<tr>
											<th>{translate("classifiers.fields.class_name")}</th>
											<th>{translate("projects.fields.description")}</th>
											<th>{translate("classifiers.fields.keywords")}</th>
										</tr>
									</thead>
									<tbody>
										{record?.intermediatory_classes?.map(
											(classItem: any, classIndex: number) => (
												<tr key={classIndex}>
													<td className="align-baseline">{classItem?.name}</td>
													<td className="align-baseline">
														{classItem?.description}
													</td>
													<td className="align-baseline">
														<Table>
															<tbody className="flex gap-4 items-center">
																<div className="flex flex-col items-center">
																	{openRows[classIndex] ? (
																		record?.intermediatory_class_to_keyword_configs
																			?.filter(
																				(group: any) =>
																					group.class_id === classItem?.id
																			)
																			.map(
																				(
																					keywordGroup: any,
																					keywordIndex: number
																				) => (
																					<>
																						<tr key={keywordIndex}>
																							<td>
																								<TextInput
																									placeholder="Keywords"
																									value={keywordGroup.musts}
																									contentEditable={false}
																								/>
																							</td>
																						</tr>
																						{record
																							?.intermediatory_class_to_keyword_configs
																							?.length > 0 &&
																							keywordIndex <
																								record.intermediatory_class_to_keyword_configs.filter(
																									(group: any) =>
																										group.class_id ===
																										classItem?.id
																								).length -
																									1 && (
																								<td className="text-center">
																									{translate("classifiers.or")}
																								</td>
																							)}
																					</>
																				)
																			)
																	) : (
																		<>
																			{
																				record?.intermediatory_class_to_keyword_configs?.filter(
																					(group: any) =>
																						group.class_id === classItem?.id
																				).length
																			}{" "}
																			{translate(
																				"classifiers.keyword_configurations"
																			)}
																		</>
																	)}
																</div>
																{record?.intermediatory_class_to_keyword_configs?.filter(
																	(group: any) =>
																		group.class_id === classItem?.id
																).length > 0 && (
																	<ActionIcon
																		color="dark"
																		variant="light"
																		onClick={() => toggleRow(classIndex)}
																	>
																		{openRows[classIndex] ? (
																			<IconChevronUp size={16} />
																		) : (
																			<IconChevronDown size={16} />
																		)}
																	</ActionIcon>
																)}
															</tbody>
														</Table>
													</td>
												</tr>
											)
										)}
									</tbody>
								</Table>
							</Container>
						</Accordion.Panel>
					</Accordion.Item>
				</Accordion>
			</div>
			<ClassifierRunModal
				opened={opened}
				setOpened={setOpened}
				classifierDetail={record}
				refetch={refetch}
			/>
		</Show>
	);
}
