import { Container, Group, Title, Tooltip } from "@mantine/core";
import { useTranslate } from "@refinedev/core";
import { IconInfoCircle } from "@tabler/icons";
import { DateField, TextField } from "@refinedev/mantine";
import React from "react";
import { formatNumber, statusTextStyle } from "src/utils";

interface Props {
	record: any;
}

const GatherViewStatus: React.FC<Props> = ({ record }) => {
	const translate = useTranslate();
	return (
		<Container className="mx-0 flex flex-col my-4">
			<Group>
				<Title my="xs" order={5}>
					{translate("gathers.fields.status")}:
				</Title>
				<span className={`${statusTextStyle(record?.latest_job_run?.status)}`}>
					{record?.latest_job_run?.status
						? translate(`status.${record.latest_job_run.status}`)
						: "-"}
				</span>
			</Group>
			<Group>
				<Title my="xs" order={5}>
					{translate("gathers.fields.started_processing_at")}:
				</Title>
				{record?.latest_job_run?.started_processing_at ? (
					<DateField
						format="LLL"
						value={record?.latest_job_run.started_processing_at}
					/>
				) : (
					"-"
				)}
			</Group>
			<Group>
				<Title my="xs" order={5}>
					{translate("gathers.fields.completed_at")}:
				</Title>
				{record?.latest_job_run?.completed_at ? (
					<DateField format="LLL" value={record?.latest_job_run.completed_at} />
				) : (
					"-"
				)}
			</Group>
			<Group>
				<Title my="xs" order={5}>
					{translate("buttons.delete")} {translate("projects.fields.status")}:
				</Title>
				{record?.delete_job_run ? (
					<TextField
						className={`capitalize ${statusTextStyle(record?.delete_job_run?.status === "completed_successfully" ? "deleted" : record?.delete_job_run?.status)}`}
						value={translate(
							`status.delete_status.${record.delete_job_run.status}`
						)}
					/>
				) : (
					"-"
				)}
			</Group>
			<Group>
				<Title my="xs" order={5}>
					{translate("gathers.fields.total_cost")}:
				</Title>
				{record?.latest_job_run?.total_cost
					? formatNumber(record.latest_job_run.total_cost)
					: "-"}
			</Group>
			<Group>
				<Title my="xs" order={5}>
					<Tooltip
						label={translate("gathers.fields.result_count_tooltip")}
						width={200}
						multiline
					>
						<span>
							<IconInfoCircle size={14} />
						</span>
					</Tooltip>
					&nbsp;
					{translate("gathers.fields.result_count")}:
				</Title>
				{record?.latest_job_run?.gather_result_count
					? record.latest_job_run.gather_result_count
					: "-"}
			</Group>
			<Group>
				<Title my="xs" order={5}>
					<Tooltip
						label={translate("gathers.fields.error_count_tooltip")}
						width={200}
						multiline
					>
						<span>
							<IconInfoCircle size={14} />
						</span>
					</Tooltip>
					&nbsp;
					{translate("gathers.fields.error_count")}:
				</Title>
				{record?.latest_job_run?.gather_normalise_error_count
					? record.latest_job_run.gather_normalise_error_count
					: "-"}
			</Group>
		</Container>
	);
};

export default GatherViewStatus;
