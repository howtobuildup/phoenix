"""Tests for write_manual_upload_to_gather_batches manual upload gathers."""
import datetime
import json
from unittest import mock

import pandas as pd
from prefect.logging import disable_run_logger as disable_prefect_run_logger

from phiphi import utils
from phiphi.api.projects.gathers import schemas as gathers_schemas
from phiphi.api.projects.gathers.manual_upload import schemas as manual_upload_schemas
from phiphi.pipeline_jobs.gathers import manual_upload
from phiphi.tests.pipeline_jobs.gathers.manual_upload import conftest

utils.init_logging()


@mock.patch("phiphi.pipeline_jobs.utils.write_data")
def test_write_manual_upload_to_gather_batches(
    write_data_mock,
    tmpdir,
    patch_settings,
):
    """Test write_manual_upload_to_gather_batches function."""
    input_df = conftest.create_mock_manual_upload_df(13)
    batch_size = 2
    batch_of_batches_size = 2
    file_path = str(tmpdir.join("manual_upload_data_1.csv"))
    input_df.to_csv(file_path, index=False)

    # Prepare the manual upload gather response
    manual_upload_gather = manual_upload_schemas.ManualUploadGatherResponse(
        id=42,
        name="Manual Upload",
        file_url=file_path,
        file_size=1000,
        uploaded_file_name="manual_upload_data_1.csv",
        file_row_count=13,
        created_at=datetime.datetime.now(),
        updated_at=datetime.datetime.now(),
        project_id=1,
        child_type=gathers_schemas.ChildTypeName.manual_upload,
    )

    # Disable Prefect logging
    with disable_prefect_run_logger():
        # Call the function
        scrape_response = manual_upload.write_manual_upload_to_gather_batches(
            manual_upload_gather=manual_upload_gather,
            job_run_id=1,
            bigquery_dataset="test_dataset",
            bigquery_table="test_table",
            batch_size=batch_size,
            batch_of_batches_size=batch_of_batches_size,
        )

    # Assertions
    assert scrape_response.total_items == input_df.shape[0]
    assert scrape_response.total_batches == 7
    assert scrape_response.total_cost == 0

    assert write_data_mock.call_count == 4

    # Check the first call (first batch)
    first_call_df = write_data_mock.call_args_list[0][0][0]
    assert len(first_call_df) == 2
    assert first_call_df["batch_id"].tolist() == [0, 1]
    assert first_call_df["gather_id"].tolist() == [42, 42]

    # Validate JSON data in the first batch
    first_batch_json_data = json.loads(first_call_df["json_data"].iloc[0])
    input_df_from_json = pd.DataFrame(first_batch_json_data)
    # Have to massage the datetime
    input_df_from_json["message_datetime"] = input_df_from_json["message_datetime"].astype(
        "datetime64[ms, UTC]"
    )  # type: ignore[call-overload]
    assert input_df_from_json.equals(input_df[:2])

    # Check the last call (last batch)
    last_call_df = write_data_mock.call_args_list[3][0][0]
    assert len(last_call_df) == 1
    assert last_call_df["batch_id"].tolist() == [6]
    assert last_call_df["gather_id"].tolist() == [42]

    # Validate JSON data in the last batch
    last_batch_json_data = json.loads(last_call_df["json_data"].iloc[0])
    input_df_from_json = pd.DataFrame(last_batch_json_data)
    # Have to massage the datetime
    input_df_from_json["message_datetime"] = input_df_from_json["message_datetime"].astype(
        "datetime64[ms, UTC]"
    )  # type: ignore[call-overload]
    # Need to drop the index
    expected_df = input_df[-1:].reset_index(drop=True)
    assert input_df_from_json.equals(expected_df)
