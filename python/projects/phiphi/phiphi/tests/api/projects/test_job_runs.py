"""Test job runs routes."""
import datetime

from fastapi.testclient import TestClient

from phiphi.api.projects.job_runs import crud, schemas
from phiphi.seed import job_runs as seed_job_runs

CREATED_TIME = "2024-04-01T12:00:01"
UPDATE_TIME = "2024-04-01T12:00:02"


def test_get_job_runs(client: TestClient, reseed_tables) -> None:
    """Test getting job runs."""
    response = client.get("/projects/1/job_runs/?start=0&end=10")
    assert response.status_code == 200
    job_runs = response.json()
    length = 10
    assert len(job_runs) == length
    # Assert desc id
    assert job_runs[length - 1]["id"] < job_runs[0]["id"]

    response = client.get("/projects/2/job_runs/")
    assert response.status_code == 200
    job_runs = response.json()
    assert len(job_runs) == 1
    assert job_runs[0]["id"] == 6


def test_get_job_runs_by_type(client: TestClient, reseed_tables) -> None:
    """Test getting job runs."""
    response = client.get("/projects/1/job_runs/?foreign_job_type=gather")
    assert response.status_code == 200
    job_runs = response.json()
    assert len(job_runs) == 1
    assert job_runs[0]["id"] == 2
    assert job_runs[0]["foreign_job_type"] == "gather"

    response = client.get("/projects/1/job_runs/?foreign_job_type=gather_classify_tabulate")
    assert response.status_code == 200
    job_runs = response.json()
    assert len(job_runs) == 6
    # Assert desc id
    assert job_runs[-2]["id"] == 4
    assert job_runs[-2]["foreign_job_type"] == "gather_classify_tabulate"
    assert job_runs[-1]["id"] == 1
    assert job_runs[-1]["foreign_job_type"] == "gather_classify_tabulate"


def test_get_job_runs_pagination(client: TestClient, reseed_tables) -> None:
    """Test getting job runs with pagination."""
    response = client.get("/projects/1/job_runs/?start=0&end=1")
    assert response.status_code == 200
    job_runs = response.json()
    assert len(job_runs) == 1


def test_crud_update_completed_job_run(reseed_tables) -> None:
    """Test updating a job run."""
    # 5 has not been completed
    job_run_response = seed_job_runs.SEEDED_JOB_RUNS[5]
    completed = schemas.JobRunUpdateCompleted(
        id=job_run_response.id,
        completed_at=datetime.datetime.fromisoformat(UPDATE_TIME),
        status=schemas.Status.completed_successfully,
        total_cost=2.0,
        gather_result_count=1,
    )
    job_run_completed = crud.update_job_run(reseed_tables, completed)
    assert job_run_completed.id == job_run_response.id
    assert job_run_completed.completed_at is not None
    assert job_run_completed.completed_at.isoformat() == UPDATE_TIME
    assert job_run_completed.status == schemas.Status.completed_successfully
    assert job_run_completed.total_cost == 2.0
    assert job_run_completed.gather_result_count == 1


def test_crud_update_completed_job_run_no_cost(reseed_tables) -> None:
    """Test updating a job run."""
    # 5 has not been completed
    job_run_response = seed_job_runs.SEEDED_JOB_RUNS[5]
    completed = schemas.JobRunUpdateCompleted(
        id=job_run_response.id,
        completed_at=datetime.datetime.fromisoformat(UPDATE_TIME),
        status=schemas.Status.completed_successfully,
    )
    job_run_completed = crud.update_job_run(reseed_tables, completed)
    assert job_run_completed.id == job_run_response.id
    assert job_run_completed.completed_at is not None
    assert job_run_completed.completed_at.isoformat() == UPDATE_TIME
    assert job_run_completed.status == schemas.Status.completed_successfully
    assert job_run_completed.total_cost is None
    assert job_run_completed.gather_result_count is None
