"""Integration tests for Danek Facebook search API.

This test script has real costs associated with it. It should be run sparingly, used only for the
development of key integration functions, and not as part of the regular test suite.
"""
import datetime

import pytest

from phiphi import config
from phiphi.pipeline_jobs.gathers.danek import facebook_search_scraper


@pytest.mark.danek_integration
def test_get_facebook_posts():
    """Integration test for get_facebook_posts function.

    This test verifies:
    1. The function can make a request to the actual API
    2. Returns results with the expected structure
    3. Handles pagination correctly
    """
    # Retrieve API token from environment variable
    api_token = config.settings.DANEK_API_TOKENS.get("main")

    # Skip test if no token is available
    if not api_token:
        pytest.skip("No API token provided for Danek Facebook Scraper API")

    # Prepare search parameters
    request_params = facebook_search_scraper.SearchRequestParams(
        token=api_token,
    )

    search_params = facebook_search_scraper.SearchParams(
        search_query="tech innovation",
        start_datetime=datetime.datetime(2024, 1, 1, 0, 0, 0, tzinfo=datetime.timezone.utc),
        end_datetime=datetime.datetime(2024, 1, 2, 23, 0, 0, tzinfo=datetime.timezone.utc),
        recent_posts=False,
    )
    stop_requesting_pages_after_message_result_count = 20

    # Call the function
    search_results = facebook_search_scraper.get_facebook_posts(
        request_params,
        search_params,
        stop_requesting_pages_after_message_result_count=stop_requesting_pages_after_message_result_count,
    )

    # Assertions to validate the results
    assert search_results is not None, "Search results should not be None"

    # Check result structure
    assert hasattr(search_results, "data"), "Results should have a 'data' attribute"
    assert hasattr(search_results, "page_count"), "Results should have a 'page_count' attribute"
    assert hasattr(
        search_results, "result_count"
    ), "Results should have a 'result_count' attribute"

    # Validate data
    assert isinstance(search_results.data, list), "Data should be a list"
    assert search_results.result_count == len(
        search_results.data
    ), "Result count should match data length"
    assert search_results.page_count >= 1, "Page count should be at least 1"
    assert (
        search_results.result_count >= stop_requesting_pages_after_message_result_count
    ), "Result count should be greater than the stop_requesting_pages_after_message_result_count"
    assert search_results.error_result is None, "No error should be present"

    for result in search_results.data:
        # Example of checking first result structure (adjust based on actual API response)
        assert isinstance(result, dict), "Individual results should be dictionaries"

        # Add more specific checks based on expected result structure
        expected_keys = ["post_id", "message", "timestamp"]
        for key in expected_keys:
            assert key in result, f"First result should contain '{key}'"


@pytest.mark.danek_integration
def test_get_facebook_posts_error_handling():
    """Test error handling for get_facebook_posts function.

    Verifies that the function handles invalid parameters gracefully.
    """
    # Prepare invalid search parameters
    request_params = facebook_search_scraper.SearchRequestParams(
        token="invalid_token",
    )

    search_params = facebook_search_scraper.SearchParams(
        search_query="",  # Empty query
        start_datetime=datetime.datetime(2024, 1, 1, 0, 0, 0, tzinfo=datetime.timezone.utc),
        end_datetime=datetime.datetime(2024, 1, 2, 23, 0, 0, tzinfo=datetime.timezone.utc),
    )

    # Call the function and check it doesn't raise unhandled exceptions
    try:
        search_results = facebook_search_scraper.get_facebook_posts(request_params, search_params)

        # Expect either empty results or a specific error handling
        assert search_results.result_count == 0, "Invalid request should return empty results"
        assert len(search_results.data) == 0, "Invalid request should return no data"
        assert search_results.error_result, "Invalid request should have error results"
        assert search_results.error_result.error, "Error message should be present"
        assert "401" in search_results.error_result.error, "Error message should contain '401'"
        assert search_results.error_result.url, "Error URL should be present"
        assert search_results.error_result.payload, "Error payload should be present"

    except Exception as e:
        pytest.fail(f"get_facebook_posts should handle errors gracefully: {e}")
