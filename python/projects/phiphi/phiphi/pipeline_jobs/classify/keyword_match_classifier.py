"""Keyword match classifier module."""
import dataclasses
import re

import prefect
from google.cloud import bigquery

from phiphi.api.projects.classifiers.keyword_match import schemas
from phiphi.pipeline_jobs import constants as pipeline_jobs_constants


@dataclasses.dataclass
class KeywordMatchRegexConfig:
    """Configuration for a keyword match regex."""

    raw_must: str
    regex: str
    is_quoted: bool


PRE_WORD_BOUNDARY_STR = "(^|[^\\p{L}0-9_])"
POST_WORD_BOUNDARY_STR = "([^\\p{L}0-9_]|$)"


def create_keyword_match_regex_config(raw_must: str) -> list[KeywordMatchRegexConfig]:
    r"""Create a list of KeywordMatchRegexConfig from a raw must string.

    This function parses `raw_must` for tokens. Tokens can be separated by whitespace
    or enclosed in double-quotes to keep multi-word tokens together. For tokens
    enclosed in quotes, `is_quoted` is set to True, and we do not add word-boundaries
    in the resulting regex. For unquoted tokens, `is_quoted` is set to False and
    word-boundaries (`\\b`) are used in the regex to match whole words.

    Args:
        raw_must: A string containing quoted or unquoted keywords
        (e.g., 'apples "bananas oranges"').

    Returns:
        A list of KeywordMatchRegexConfig objects.
    """
    # Matches either a quoted group ("...") or an unquoted sequence of non-whitespace
    pattern = re.compile(r'"([^"]+)"|(\S+)')
    matches = pattern.findall(raw_must)

    configs = []
    for quoted, unquoted in matches:
        if quoted:
            pattern = re.escape(quoted)
            # Quoted token -> no word boundaries, exact match
            configs.append(
                KeywordMatchRegexConfig(
                    raw_must=quoted,
                    # case-insensitive and checks for language agnostic word boundaries +
                    # start/end of sentence
                    regex=rf"(?is){PRE_WORD_BOUNDARY_STR}{pattern}{POST_WORD_BOUNDARY_STR}",
                    is_quoted=True,
                )
            )
        else:
            pattern = re.escape(unquoted)
            # Unquoted token -> use word boundaries
            configs.append(
                KeywordMatchRegexConfig(
                    raw_must=unquoted,
                    # case-insensitive and checks for language agnostic word boundaries +
                    # start/end of sentence
                    regex=rf"(?is){PRE_WORD_BOUNDARY_STR}{pattern}{POST_WORD_BOUNDARY_STR}",
                    is_quoted=False,
                )
            )

    return configs


@prefect.task
def classify(
    classifier: schemas.KeywordMatchClassifierPipeline, bigquery_dataset: str, job_run_id: int
) -> None:
    """Classify messages using keyword match classifier through BigQuery query."""
    client = bigquery.Client()
    source_table_name = (
        f"{bigquery_dataset}."
        f"{pipeline_jobs_constants.DEDUPLICATED_GENERALISED_MESSAGES_TABLE_NAME}"
    )
    destination_table_name = (
        f"{bigquery_dataset}.{pipeline_jobs_constants.CLASSIFIED_MESSAGES_TABLE_NAME}"
    )

    # Prepare query parameters
    # Note: The query parameters are used to prevent SQL injection
    query_parameters = [
        bigquery.ScalarQueryParameter("classifier_id", "INT64", classifier.id),
        bigquery.ScalarQueryParameter(
            "classifier_version_id", "INT64", classifier.latest_version.version_id
        ),
        bigquery.ScalarQueryParameter("job_run_id", "INT64", job_run_id),
    ]

    unclassified_messages_query = f"""
        WITH unclassified_messages AS (
            SELECT
                src.phoenix_platform_message_id,
                src.pi_text  -- Include pi_text so it can be used in WHERE conditions
            FROM
                `{source_table_name}` AS src
            LEFT JOIN
                `{destination_table_name}` AS dst
            ON
                src.phoenix_platform_message_id = dst.phoenix_platform_message_id
                AND dst.classifier_id = @classifier_id
                AND dst.classifier_version_id = @classifier_version_id
            WHERE
                dst.phoenix_platform_message_id IS NULL
        )
    """

    select_statements = []
    # config_index is used to create unique query parameter names for each class and keyword
    config_index = 0
    for config in classifier.latest_version.params["class_to_keyword_configs"]:
        if not config["musts"]:
            continue

        keywords = create_keyword_match_regex_config(config["musts"])
        keyword_params = []
        for i, keyword_config in enumerate(keywords):
            param_name = f"config_{config_index}_keyword_{i}"

            query_parameters.append(
                bigquery.ScalarQueryParameter(param_name, "STRING", keyword_config.regex)
            )
            keyword_params.append(f"REGEXP_CONTAINS(pi_text, @{param_name})")

        # Combine keyword conditions
        combined_conditions = " AND ".join(keyword_params)
        class_name_param = f"class_name_{config_index}"
        query_parameters.append(
            bigquery.ScalarQueryParameter(class_name_param, "STRING", config["class_name"])
        )

        select_statements.append(
            f"""
            SELECT
                @classifier_id AS classifier_id,
                @classifier_version_id AS classifier_version_id,
                @{class_name_param} AS class_name,
                phoenix_platform_message_id,
                @job_run_id AS job_run_id
            FROM
                unclassified_messages
            WHERE
                {combined_conditions}
            """
        )
        config_index += 1

    # Construct the final query
    union_query = " UNION ALL ".join(select_statements)
    query = f"""
        INSERT INTO `{destination_table_name}`
        (
            classifier_id,
            classifier_version_id,
            class_name,
            phoenix_platform_message_id,
            job_run_id
        )
        {unclassified_messages_query}
        {union_query}
    """

    # Prepare the job configuration with query parameters
    job_config = bigquery.QueryJobConfig(query_parameters=query_parameters)

    # Execute the query
    query_job = client.query(query, job_config=job_config)
    query_job.result()
