"""Integration for the Danek Facebook search scraper."""
import dataclasses
import datetime
import time
from enum import Enum
from typing import Optional

import requests


class SearchType(str, Enum):
    """Enum for Danek API type."""

    PAGES = "PAGES_TAB"
    PLACES = "PLACES_TAB"
    POSTS = "POSTS_TAB"
    GROUPS = "GROUPS_TAB"
    VIDEOS = "VIDEOS_TAB"
    EVENTS = "EVENTS_TAB"


@dataclasses.dataclass
class SearchParams:
    """Dataclass for search request parameters."""

    search_query: str
    start_datetime: Optional[datetime.datetime] = None
    end_datetime: Optional[datetime.datetime] = None
    recent_posts: bool = True
    location_uid: Optional[str] = None
    proxy_code: Optional[str] = None
    search_type: SearchType = SearchType.POSTS


@dataclasses.dataclass
class SearchRequestParams:
    """Dataclass for request parameters."""

    token: str
    url: str = "https://fb-scraper-api.fly.dev/api/custom/build_up/search"


@dataclasses.dataclass
class SearchError:
    """Dataclass for search error."""

    error: str
    url: str
    payload: dict


@dataclasses.dataclass
class SearchResults:
    """Dataclass for aggregated search results."""

    data: list
    page_count: int
    result_count: int
    error_result: Optional[SearchError] = None


def _prepare_search_request(
    request_params: SearchRequestParams, search_params: SearchParams, cursor: Optional[str] = None
) -> tuple:
    """Prepare URL, headers, and payload for the search request.

    Args:
        request_params: Configuration for the API request
        search_params: Search criteria
        cursor: Pagination cursor

    Returns:
        Tuple of (url, headers, payload)
    """
    url = request_params.url
    headers = {
        "Authorization": f"Bearer {request_params.token}",
        "Content-Type": "application/json",
    }
    search_config = {}

    search_config_params = ["recent_posts", "location_uid", "proxy_code"]
    for param in search_config_params:
        if getattr(search_params, param):
            search_config[param] = getattr(search_params, param)

    if search_params.start_datetime:
        search_config["start_date"] = search_params.start_datetime.isoformat()

    if search_params.end_datetime:
        search_config["end_date"] = search_params.end_datetime.isoformat()

    payload = {
        "query": search_params.search_query,
        "cursor": cursor,
        "search_type": search_params.search_type.value,
        "search_config": search_config,
    }
    return url, headers, payload


def make_single_request(
    url: str, headers: dict, payload: dict, max_retries: int = 3, backoff_factor: float = 1.0
) -> tuple[list, str | None]:
    """Makes a single request to the Danek API, with retry logic.

    Args:
        url: Complete endpoint URL.
        headers: HTTP request headers.
        payload: JSON body to post.
        max_retries: Maximum number of retry attempts for transient errors.
        backoff_factor: Factor by which the sleep time increases
            after each retry (exponential backoff).

    Returns:
        A tuple containing the search results (list) and the cursor (str) for pagination.

    Raises:
        RuntimeError: For any request or response parsing issues after exhausting retries.
    """
    for attempt in range(max_retries):
        status_code = None
        response_json = None
        try:
            response = requests.post(url, headers=headers, json=payload)
            status_code = response.status_code
            response_json = response.json()
            response.raise_for_status()

            # Attempt to parse the JSON.
            results = response_json.get("results", [])
            cursor = response_json.get("cursor")
            return results, cursor

        except requests.exceptions.HTTPError as exc:
            # Check if it's a server error (5xx). If so, retry; otherwise, re-raise.
            if status_code and 500 <= status_code < 600 and attempt < max_retries:
                sleep_time = backoff_factor * (2**attempt)
                time.sleep(sleep_time)
                continue
            else:
                raise RuntimeError(
                    f"HTTP error occurred (status code {status_code}): {exc}"
                ) from exc

        except requests.exceptions.RequestException as exc:
            # Covers various connection-related errors (ConnectionError, Timeout, etc.)
            # Retry on the assumption it's transient.
            if attempt < max_retries:
                sleep_time = backoff_factor * (2**attempt)
                time.sleep(sleep_time)
                continue
            else:
                raise RuntimeError(f"Network or connection error occurred: {exc}") from exc

        except ValueError as exc:
            # JSON parsing error: typically not transient, so we do not retry.
            raise RuntimeError(f"Failed to parse JSON response: {exc}") from exc

    # If we exited the loop without returning or raising, raise a generic error.
    raise RuntimeError("Failed to complete request after all retries.")


def get_facebook_posts(
    request_params: SearchRequestParams,
    search_params: SearchParams,
    stop_requesting_pages_after_message_result_count: Optional[int] = None,
) -> SearchResults:
    """Retrieve all Facebook posts matching search criteria.

    Args:
        request_params: Request configuration
        search_params: Search criteria
        stop_requesting_pages_after_message_result_count: Optional limit to stop requesting pages
            after a certain number of results. If None, all pages will be requested.

    Returns:
        SearchResults containing all matching posts
    """
    all_data: list[dict] = []
    page_count = 0
    cursor = None

    url, headers, payload = _prepare_search_request(request_params, search_params, cursor)

    while True:
        page_count += 1
        payload["cursor"] = cursor
        try:
            search_results, cursor = make_single_request(url, headers, payload)
        except RuntimeError as exc:
            return SearchResults(
                data=all_data,
                page_count=page_count,
                result_count=len(all_data),
                error_result=SearchError(
                    error=str(exc),
                    url=url,
                    payload=payload,
                ),
            )
        all_data.extend(search_results)

        if (
            stop_requesting_pages_after_message_result_count
            and len(all_data) >= stop_requesting_pages_after_message_result_count
        ):
            break

        if not cursor:
            break

    return SearchResults(
        data=all_data, page_count=page_count, result_count=len(all_data), error_result=None
    )
