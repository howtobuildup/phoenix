"""Dependencies for routes for the project."""
from typing import Annotated

import fastapi

from phiphi.api import deps, exceptions
from phiphi.api.projects import crud, schemas


def get_project_detail(session: deps.SessionDep, project_id: int) -> schemas.ProjectDetail:
    """Get the project detail."""
    project = crud.get_project(session, project_id)
    if project is None:
        raise exceptions.ProjectNotFound()
    return project


ProjectDetailDep = Annotated[schemas.ProjectDetail, fastapi.Depends(get_project_detail)]
